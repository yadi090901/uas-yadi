from django import forms
from .models import ContactModel


class ContactForm(forms.ModelForm):
    class Meta:
        model   = ContactModel
        fields   = [
            'nama_lengkap',
            'email',
            'subject',
            'message',
        ]


# class ContactForm(forms.Form):
#     nama_lengkap        = forms.CharField(max_length=200)
#     email               = forms.CharField(max_length=200)
#     subject             = forms.CharField(max_length=250)
#     message             = forms.CharField(max_length=250, widget=forms.Textarea)