from django.db import models

# Create your models here.
class ContactModel(models.Model):
    nama_lengkap        = models.CharField(max_length=200)
    email               = models.CharField(max_length=200)
    subject             = models.CharField(max_length=250)
    message             = models.TextField()


    def __str__(self):
        return"{}.{}".format(self.id, self.nama_lengkap)