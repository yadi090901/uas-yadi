from django.shortcuts import render
from .models import ContactModel
from .forms import ContactForm


# Create your views here.

def contact(request):
    models = ContactModel.objects.all()
    form = ContactForm(request.POST or None)
    context = {
        'form': form,
        'model': models,
    }

    if request.method == 'POST':
        if form.is_valid():
            form.save()

            # ContactModel.objects.create(
            #             nama_lengkap      = request.POST.get('nama_lengkap'),
            #             email             = request.POST.get('email'),
            #             subject          = request.POST.get('subject'),
            #             message           = request.POST.get('message'),
            # )




    return  render(request, 'contact/contact.html', context)